package algorithm_test

import (
	"fmt"
	"rangelist/algorithm"
	"testing"
)

func TestRangeList(t *testing.T) {
	// go test -v rangelist_test.go -run TestRangeList
	var CurrentRangList algorithm.RangeList

	CurrentRangList.Add([2]int{1, 5})
	CurrentRangList.Print()
	fmt.Println()
	CurrentRangList.Add([2]int{10, 20})
	CurrentRangList.Print()
	fmt.Println()
	CurrentRangList.Add([2]int{20, 20})
	CurrentRangList.Print()
	fmt.Println()
	CurrentRangList.Add([2]int{20, 21})
	CurrentRangList.Print()
	fmt.Println()
	CurrentRangList.Add([2]int{2, 4})
	CurrentRangList.Print()
	fmt.Println()
	CurrentRangList.Add([2]int{3, 8})
	CurrentRangList.Print()
	fmt.Println()
	CurrentRangList.Remove([2]int{10, 10})
	CurrentRangList.Print()
	fmt.Println()
	CurrentRangList.Remove([2]int{10, 11})
	CurrentRangList.Print()
	fmt.Println()
	CurrentRangList.Remove([2]int{15, 17})
	CurrentRangList.Print()
	fmt.Println()
	CurrentRangList.Remove([2]int{3, 19})
	CurrentRangList.Print()
	fmt.Println()

}
