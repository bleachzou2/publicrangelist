package algorithm_test

import (
	"rangelist/algorithm"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestBisectLeft(t *testing.T) {
	// go test -v bisect_test.go -run TestBisectLeft

	var data []int
	data = append(data, 3)
	data = append(data, 4)
	data = append(data, 5)
	data = append(data, 5)
	data = append(data, 5)
	data = append(data, 10)
	data = append(data, 18)
	currentResult := algorithm.BisectLeft(data, 5)
	assert.Equal(t, 2, currentResult)
	currentResult = algorithm.BisectRight(data, 5)
	assert.Equal(t, 5, currentResult)
}
