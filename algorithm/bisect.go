package algorithm

import(
	"sort"
)

func BisectLeft(data []int, target int) int {
	return BisectLeftRange(data, target, 0, len(data))
}

func BisectLeftRange(data []int, target int, low, high int) int {
	currentRange := data[low:high]
	return sort.Search(len(currentRange), func(i int) bool {
		return currentRange[i] >= target
	})
}

func BisectRight(data []int, target int) int {
	return BisectRightRange(data, target, 0, len(data))
}

func BisectRightRange(data []int, target int, low, high int) int {
	currentRange := data[low:high]
	return sort.Search(len(currentRange), func(i int) bool {
		return currentRange[i] > target
	})
}