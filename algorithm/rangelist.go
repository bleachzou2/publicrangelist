package algorithm

import "fmt"

type RangeList struct {
	//TODO: implement
	data []int
}

func (rangeList *RangeList) Add(rangeElement [2]int) error {
	//TODO: implement return nil
	ivs := rangeList.data
	// ivs := make([]int, len(rangeList.data))
	left := rangeElement[0]
	right := rangeElement[1]
	ilo, ihi := BisectLeft(ivs, left), BisectRight(ivs, right)
	if ilo%2 == 1 {
		ilo -= 1
		left = ivs[ilo]
	}
	if ihi%2 == 1 {
		right = ivs[ihi]
		ihi += 1
	}
	leftRange := ivs[:ilo]
	middleRange := []int{left, right}
	rightRange := ivs[ihi:]
	rangeList.data = append(leftRange, middleRange...)
	rangeList.data = append(rangeList.data, rightRange...)
	return nil
}

func (rangeList *RangeList) Remove(rangeElement [2]int) error {
	//TODO: implement return nil
	ivs := rangeList.data
	left := rangeElement[0]
	right := rangeElement[1]
	ilo, ihi := BisectLeft(ivs, left), BisectRight(ivs, right)

	var newData []int = make([]int, 0)
	if ilo%2 == 1 {
		ilo -= 1
		newData = append(newData, []int{ivs[ilo], left}...)

	}
	if ihi%2 == 1 {
		newData = append(newData, []int{right, ivs[ihi]}...)
		ihi += 1
	}
	leftRange := ivs[:ilo]
	rangeList.data = append(leftRange, newData...)
	rangeList.data = append(rangeList.data, ivs[ihi:]...)
	return nil
	//self.ivs = ivs[:ilo] + new + ivs[ihi:]
}

func (rangeList *RangeList) Print() error {
	//TODO: implement return nil
	for i := 0; i < len(rangeList.data); i = i + 2 {
		fmt.Printf("[%d %d)", rangeList.data[i], rangeList.data[i+1])
	}
	return nil
}
